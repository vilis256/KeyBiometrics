import java.util.*;
import java.io.*;

public class Log {
  public static void print(Object o, String msg) {
    System.out.println("<<<"+o.toString()+" : "+msg+">>>");
  }

  public static void printPlain(String msg) {
    System.out.println(msg);
  }

  public static void print(String msg) {
    System.out.println("<<<main : "+msg+">>>");
  }

  public static void printErr(Object o, Exception e) {
    System.out.println("!! >>Error in class "+o.toString()+":");
    e.printStackTrace();
    System.out.print("  << !!");
  }

  public static void printErr(Object o, Exception e, int ln) {
    System.out.println("!! >>Error in class "+o.toString()+" on line "+ln+":");
    e.printStackTrace();
    System.out.print("  << !!");
  }

  public static void SprintErr(Exception e) {
    System.out.println("!! >>Error in main:");
    e.printStackTrace();
    System.out.print("  << !!");
  }

  public static void printErr(Exception e, int ln) {
    System.out.println("!! >>Error in main on line "+ln+":");
    e.printStackTrace();
    System.out.print("  << !!");
  }

  public static void printErr(String msg) {
    System.out.println("!! >>Error in main: ");
    System.out.print(msg);
    System.out.print("  << !!");
  }

  public static void printErr(String msg,Exception e) {
    System.out.print("\n!! >>Error in main: "+msg+":");
    e.printStackTrace();
    System.out.println("  << !!");
  }

  public static void printErr(int ln, String msg, Object o) {
    System.out.println("!! >>Error in class "+o.toString() + " on line "+ln+": ");
    System.out.print(msg);
    System.out.print("  << !!");
  }

  public static void printErr(String msg, Object o) {
    System.out.println("!! >>Error in class "+o.toString() + ": ");
    System.out.print(msg);
    System.out.print("  << !!");
  }

  public static void printErr(int ln, String msg) {
    System.out.println("!! >>Error in main on line "+ln+": ");
    System.out.print(msg);
    System.out.print("  << !!");
  }

  public static void mark(int ln) {
    System.out.println("<<<Passed marker on line "+ln+">>>");
  }

  public static void mark(Object o, int ln) {
    System.out.println("<<<Passed marker on line "+ln+" in class "+o.toString()+">>>");
  }
}
