public class Encrypt{
  private static final long key = 3365084039l;


  public static String encrypt(String in){
    String out ="";
    String[] parts = in.split("~");

    for(String i:parts){
      if(!i.equals("/") && !i.equals(":") && !i.equals("\n") && !i.equals("\\+")){
        boolean c = false;

        if(! ((int)i.charAt(0) >= 48 && (int)i.charAt(0) <= 57)){
          out += "*";
          c=true;
        }
        long o;
        if(c){
          o = bSev(key) * bSev2(i);
        }
        else{
          o = bSev(key) * bSev(i);
        }
        out += hexify(o);
        //out += o;
        out += "`";
      }
      else{
        out += i;
        out += "`";
      }
    }

    return out;
  }

  public static String eString(String in){
    String out ="";
    String[] parts = in.split("");
    for(String i:parts){

        boolean c = false;

        if(! ((int)i.charAt(0) >= 48 && (int)i.charAt(0) <= 57)){
          out += "*";
          c=true;
        }
        long o;
        if(c){
          o = bSev(key) * bSev2(i);
        }
        else{
          o = bSev(key) * bSev(i);
        }
        out += hexify(o);
        out += "`";
    }

    return out;

  }

  public static String bannanas(String in){
    String out = "";
    String[] input = in.split("`");

    for(String i:input){
      if(!i.equals("/") && !i.equals(":") && !i.equals("\n") && !i.equals("\\+") && !i.equals("")){
        boolean c = false;

        if(i.charAt(0) == '*'){
          c=true;
          i = i.substring(1,i.length());
        }

        long o1 = dehexify(i);
        //long o1 = Long.parseLong(i);
        long o2 = o1 / bSev(key);

        if(c){
          out += (char)(int)bTen(o2);
        }else{
          if(o2 == 755){
            Log.print(i);
          }
          out += bTen(o2);
        }

      }
      else{
        out += i;
      }
    }

    return out;
  }

  private static long bSev2(String val){
    long rval = (long)(int)val.charAt(0);
    String xval = Long.toString(rval,7);
    return Long.parseLong(xval);
  }
  private static long bSev(String val){
    long rval = Long.parseLong(val);
    String xval = Long.toString(rval,7);
    return Long.parseLong(xval);
  }
  private static long bSev(long val){
    String xval = Long.toString(val,7);
    return Long.parseLong(xval);
  }

  private static long bTen(long val){
    String num = Long.toString(val);
    try{
      String num2 = Long.toString(Long.parseLong(num, 7), 10);
      return Long.parseLong(num2);
    }catch(NumberFormatException e){
      e.printStackTrace();
      throw e;
    }

  }
  private static long bTen(String val){
    String num = val;
    try{
      String num2 = Long.toString(Long.parseLong(num, 7), 10);
      return Long.parseLong(num2);
    }catch(NumberFormatException e){
      e.printStackTrace();
      throw e;
    }
  }

  private static String hexify(long b7){
    return Long.toHexString(b7);
  }
  private static long dehexify(String str){
    String r = "0x"+str;
    try{
      return Long.decode(r);
    }catch(NumberFormatException e){
      return bSev(128) * bSev(key);
    }
  }
}
