import ddf.minim.*;

UserDB users;
LoginMenu login;
LoginMenu newUser;
boolean loggedIn = false;
boolean mainMenu = true;
boolean loginMenu = false;
boolean nUserMenu = false;
KeyMap signature = new KeyMap();
KeyMap[] learnPattern = new KeyMap[6];
KeyRecorder loginPass = new KeyRecorder();
KeyRecorder[] nuLearn = new KeyRecorder[8];
ArrayList<Button> theButtons = new ArrayList<Button>();
ArrayList<Button> lesButtons=new ArrayList<Button>();
PImage back, failBack, home, instruct;
PFont futr;
Minim minim;
AudioPlayer[] track;
AudioPlayer[] homeSong;
int songPlaying;
SLog log;
boolean mute = true;

float px, py;


void setup() {

  fullScreen();
  //size(800,800);
  px=(width/1920.0)*1260.0;
  py=(height/1080.0)*165.0;
  futr=createFont("DS-DIGI.TTF", width/30);

  back=loadImage("GUI.png");
  failBack=loadImage("GUI_F.png");
  home = loadImage("HomePage.png");
  instruct = loadImage("Instruct.png");
  back.resize(width, height);
  failBack.resize(width, height);

  log = new SLog(40,13,px,py,5);

  log.domain("System");
  log.command("Beginning startup...");
  log.command("Loading Library KeyBiometrics...");
  log.command("Complete! | Running Setup...");

  image(back, 0, 0, width, height);
  log.show();

  log.subCmd("Fonts and Images loaded...");

  image(back, 0, 0, width, height);
  log.show();

  minim = new Minim(this);
  track = new AudioPlayer[9];
  track[0] = minim.loadFile("Track.mp3");
  track[1] = minim.loadFile("Track2.mp3");
  track[2] = minim.loadFile("Track3.mp3");
  track[3] = minim.loadFile("Track4.mp3");
  track[4] = minim.loadFile("Track5.wav");
  track[5] = minim.loadFile("Track6.wav");
  track[6] = minim.loadFile("Track7.wav");
  track[7] = minim.loadFile("Track8.wav");
  track[8] = minim.loadFile("TrackX.mp3");

  homeSong = new AudioPlayer[4];
  homeSong[0] = minim.loadFile("Home1.mp3");
  homeSong[1] = minim.loadFile("Home2.mp3");
  homeSong[2] = minim.loadFile("Home3.mp3");
  homeSong[3] = minim.loadFile("Home4.mp3");

  log.subCmd("Sound Files loaded...");
  songPlaying = 0;

  image(back, 0, 0, width, height);
  log.show();

  login = new LoginMenu("Login", false, width, height);
  newUser = new LoginMenu("Create a New User", true, width, height);

  try {
    users = new UserDB(dataPath(""));

    log.subCmd("User Data loaded...");
    image(back, 0, 0, width, height);
    log.show();
  }
  catch(IOException e) {
    Log.printErr("BAD IOEXCEPTION!!!!", e);

    log.subCmd("User Data load failed!...");
    image(back, 0, 0, width, height);
    log.show();
  }

  for (int i=0; i<6; i++) {
    nuLearn[i] = new KeyRecorder();
    learnPattern[i] = new KeyMap();
  }

  theButtons.add(new Button("Login", (int)(width-width/(2.9))-10, 10, #07CFF0));
  theButtons.add(new Button("Create User", (int)(width-width/(4)), 10, #07CFF0));
  theButtons.add(new Button("Quit", 10, 10, #07CFF0));
  theButtons.add(new Button("Reset", width/2-100, height/2, color(255, 0, 0)));
  theButtons.add(new Button("Logout", width-(width/33)*7,10,#07CFF0));
  theButtons.add(new Button("Cancel", width-(width/33)*7,10,color(255,0,0)));
  if (! mute){
    track[songPlaying].play();
  }

  if(users.ad()){
    log.msg("Setup Complete!");
    log.domain("Admin");
    log.command("_");
  }
  else{
    log.domain("System");
    log.msg("Setup Complete!");
    log.command("Waiting for user input...");
  }

  image(back, 0, 0, width, height);
  log.show();

  theButtons.get(0).activate();
  theButtons.get(1).activate();
}

void draw(){
  if (! loggedIn) {
    if (! track[songPlaying].isPlaying() && ! mute) {
      Log.print("Next Song");
      track[songPlaying].pause();
      track[songPlaying].rewind();
      songPlaying ++;

      if (songPlaying > 7) {
        songPlaying = 0;
      }

      track[songPlaying].play();
    }

    mainMenu();
  } else {
    userHome();
    theButtons.get(3).deactivate();
  }

  if (loginMenu) {
    login.display();
    theButtons.get(3).activate();
    theButtons.get(3).show();
    theButtons.get(5).show();
    if(theButtons.get(5).clicked()){
      loginPass.reset();
      login.resetAll();
      theButtons.get(0).activate();
      theButtons.get(1).activate();
      loginMenu = false;
      theButtons.get(5).deactivate();
    }
  }


  if (nUserMenu) {

    newUser.display();
    theButtons.get(5).show();
    if(theButtons.get(5).clicked()){
      loginPass.reset();
      login.resetAll();
      theButtons.get(0).activate();
      theButtons.get(1).activate();
      theButtons.get(5).deactivate();
      for(KeyRecorder i:nuLearn){
        try{
          i.reset();
        }catch(NullPointerException e){
          //ignore
        }
      }
      nUserMenu = false;
    }
    for (Button i : lesButtons) {
      i.activate();
      i.show();
    }
    for (int i=0; i<lesButtons.size(); i++) {
      if (lesButtons.get(i).clicked()) {
        newUser.clearPass(i);
        if (nuLearn[i]!=null) {
          nuLearn[i].reset();
          learnPattern[i].clear();
        }
      }
    }
  }


  theButtons.get(2).activate();
  theButtons.get(2).show();
}

void logoutMain(){
  homeSong[users.loggedInSng()].pause();
  homeSong[users.loggedInSng()].rewind();

  try{
    users.close();
  }catch(IOException e){
    //Do nada
  }
  users.logoutCurrent();

  newUser.resetAll();
  login.resetAll();

  signature.clear();
  loginPass.reset();

  theButtons.get(4).deactivate();
  theButtons.get(0).activate();
  theButtons.get(1).activate();

  for(KeyRecorder i:nuLearn){
    try{
      i.reset();
    }catch(NullPointerException e){
      //ignore
    }
  }
  for(KeyMap j:learnPattern){
    try{
      j.clear();
    }catch(NullPointerException e){
      //ignore
    }
  }

  loggedIn = false;
  mainMenu = true;

  log.domain("User");
  log.command("sudo -logout");
  log.msg("Logged out of user "+users.loggedInNm());
  log.domain("/");
  log.command("cd System");
  log.domain("System");
  log.command("Waiting for user input...");
}
void quit() {
  if (loggedIn) {
    try {
      users.close();
    }
    catch(IOException e) {
      Log.printErr("!@#$", e);
    }
  }
  exit();
}
