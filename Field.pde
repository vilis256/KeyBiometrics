class Field {
  String name;
  private boolean editable;
  private boolean canClick;
  private boolean canEdit;
  private float x, y, w, h;
  private String contents;
  private String tempContents;
  private color border;

  Field(String n, boolean e, int a, int b, int c, int d) {
    name = n;
    editable = e;
    x = a;
    y = b;
    w = c;
    h = d;
    contents = "";
    tempContents = "";
    canClick = false;
    canEdit = false;
    border = color(0);
  }

  Field(String n, boolean e, int a, int b, int c, int d, String co) {
    name = n;
    editable = e;
    x = a;
    y = b;
    w = c;
    h = d;
    contents = "";
    tempContents = co;
    canClick = false;
    canEdit = false;
    border = color(0);
  }
  float[] getDim(){
    float[] a=new float[4];
    a[0]=x;
    a[1]=y;
    a[2]=w;
    a[3]=h;
    return a;
  }

  void show() {
    fill(0);
    textSize(20);

    if (name != "") {
      //fill(255,0,0);
      text(name + ":", x, y-30);
      //fill(0);
    }
    if (! editable) {
      text(contents, x, y-30);
      fill(0);
    } else {
      canClick = true;
      fill(255);
      strokeWeight(2);
      stroke(border);
      rect(x, y, w, h);
      strokeWeight(1);

      fill(100,100,150);
      text(tempContents,x+5,y+3);

      fill(0);
      text(contents, x+5, y+3);
      stroke(0);
    }
  }

  void hide() {
    canClick = false;
    canEdit = false;
  }

  void setEdit(boolean a) {
    if (canClick) {
      canEdit = a;

      if (canEdit) {
        border = color(200,20,20);


      } else {
        border = color(0);
      }
    }
  }

  boolean edit() {
    return canEdit;
  }

  void add(char let) {
    contents += let;
  }
  void del() {
    if (contents.length()>0)
      contents = contents.substring(0, contents.length()-1);
  }

  boolean inside(float xI, float yI) {
    float toX = xI-x;
    float toY = yI-y;
    if (toX < 0 || toY <0 || toX > w || toY > h+10) {
      return false;
    }

    return true;
  }

  void translator(float xT, float yT) {
    x += xT;
    y += yT;
  }

  String getContents() {
    return contents;
  }

  void erase(){
    contents = "";
  }

  //boolean stCont() {
  //  return tempContents;
  //}
}
