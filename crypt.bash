#!/bin/bash
loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $loc/debug

javac *.java
java Debugger $1
