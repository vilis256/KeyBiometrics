#!/bin/bash
myos=`uname`
loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $loc


if [ $# == 1 ]; then
  git add .
  git commit -m $1
  git push origin master
else
  git checkout -b $1
  git add .
  git commit -m $2
  git push origin $1
fi
