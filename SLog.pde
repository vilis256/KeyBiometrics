class SLog{
  private String domain;
  private String line;

  private ArrayList<String> lines;
  private ArrayList<String> domains;

  private int lineCount;
  private int lineLoc;
  private int lineSee;

  private final int maxCharCount;
  private final int maxLineCount;

  private float xLoc;
  private float yLoc;
  private int theSize;

  private int scroll;
  private int dec;

  private color c;

  //Constructor
  public SLog(int w,int h,float x,float y,int tS){
    maxCharCount = w;
    maxLineCount = h;
    xLoc = x;
    yLoc = y;
    theSize = tS;
    domain = "";
    lineLoc = 0;
    lineSee = 0;
    line = "";
    lineCount = 0;
    lines = new ArrayList<String>();
    domains = new ArrayList<String>();
    scroll = 0;
    dec = -1;
    c=color(45, 200, 210);
  }
  
  //Message display modes
  void command(String txt){
      line = txt;

      if(line.length()+domain.length() > maxCharCount){
        line = line.substring(0,48) + "\n\t" + line.substring(48);
      }

      lines.add(line);
      domains.add(dF(domain));
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }
  void msg(String txt){
      line = txt;

      if(line.length()+domain.length() > maxCharCount){
        try{
          line = line.substring(0,48) + "\n\t" + line.substring(48);
        }catch(StringIndexOutOfBoundsException e){
          
        }
      }

      lines.add(line);
      domains.add(dF(domain+"Log"));
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }
  void subCmd(String txt){
      line = txt;

      if(line.length()+domain.length() > maxCharCount){
        line = line.substring(0,48) + "\n\t" + line.substring(48);
      }

      lines.add(line);
      domains.add("\t> ");
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }
  
  //Set domain
  void domain(String d){
    domain = d;
  }
  
  //Change text color
  void changeC(color c1){
    c=c1;
  }
  
  //Display Console
  void show(){
    textFont(futr);
    textSize(width/120);

    fill(c);
    for(int i=scroll;i<lines.size();i++){
      if(! (((yLoc-10)+((i-scroll)*(width/120)+10)) > ((yLoc-10)+(maxLineCount)*(width/120)+10))){
        text(domains.get(i) + lines.get(i), xLoc, ((yLoc-10)+((i-scroll)*(width/120)+10)));
      }
    }
  }
  
  //Add letters to current line
  void type(char let){
    if(lineCount-scroll > maxLineCount){
      scroll = lineCount-2;
    }
    deleteU();

    line += let;
    line += "_";
    if(line.length()+domain.length() > maxCharCount){
      remEsc();
      line = line.substring(0,40) + "\n\t" + line.substring(40);
    }
    lines.set(lineLoc-1,line);
    //Log.print(""+lineLoc);
  }
  
  //Do something based on an inputed command
  void process(){
    
   //Cut up parts of a command
   String[] parts = line.substring(0,line.length()-1).split(" ");
   String cmd = parts[0];
   Log.print("Running command"+cmd);
   String[] args;

   if(parts.length>1){
     args = Arrays.copyOfRange(parts,1,parts.length);;
   }else{
     args = null;
   }

   //Action for "Login" command
   if(cmd.equals("login")){
     Log.print("Running login cmd...");
     
     //Attempt to login
     try{
       users.loginTo(args[0]);
     }catch(NullPointerException e){
       String t = domain;
       domain("Error");
       msg("User does not exist");
       domain(t);
       command("_");
       return;
     }
     
     loggedIn = true;
     
     //Play the correct song
     if (users.getErr().equals("Nil") && ! mute) {
       track[songPlaying].pause();
       homeSong[users.loggedInSng()].rewind();
       homeSong[users.loggedInSng()].play();
     } else if (! mute) {
       track[songPlaying].pause();
       track[8].play();
     }
     
     //Display result
     clear();
     domain("System");
     msg("Login Success! Welcome " + users.loggedInNm());
     command("cd User");

     if(users.getErr().equals("Nil")){
       domain("User");
       command("_");
     }
     else{
       domain("Error");
       msg(users.getErr());
     }

     if (users.loggedInNm().equals("Joe Smith")) {
       clear();
       domain("Error");
       msg(users.getErr());
       domain("System");
       command("_");
     }
   }
   
   //Action for "Click" command
   else if(cmd.equals("click")){
     //Click a button
     theButtons.get(Integer.parseInt(args[0])).forceClick();
   }
   
   //Action for "Logout" command
   else if(cmd.equals("logout")){
     //Switch songs
     if(loggedIn){
       if(! mute){
         homeSong[users.loggedInSng()].pause();
         track[songPlaying].play();
       }
       //Logout
       users.logoutCurrent();
     }
     else{
       msg("Not logged in to any user");
       command("_");
       return;
     }
     
     //Display Results
     loggedIn=false;
     clear();
     domain("User");
     msg("Logging out...");
     domain("System");
     command("_");
   }
   
   //Action for "Play" command
   else if(cmd.equals("play")){
     
     //Play a homeSong
     if(args[0].equals("-s")){
       playSong(args[1]);
       return;
     }
     
     //To high of an index
     if(Integer.parseInt(args[0])>track.length){
       msg("We don't have that many songs");
       command("_");
       return;
     }
     
     //Stop current song and play new one
     if(loggedIn){
       homeSong[users.loggedInSng()].pause();
       track[Integer.parseInt(args[0])-1].rewind();
       track[Integer.parseInt(args[0])-1].play();
     }
     else{
       track[songPlaying].pause();
       songPlaying = Integer.parseInt(args[0])-1;
       Log.print(songPlaying+"");
       track[songPlaying].play();
     }

     //Display results
     String tem = domain;
     domain("System");
     msg("Playing song #"+args[0]);
     domain(tem);
     command("_");
   }
   
   //Action for "Quit" command
   else if(cmd.equals("quit")){
     //Display process
     msg("Exiting program...");
     show();
     //Quit
     quit();
   }
   
   //Action for "Encrypt" command
   else if(cmd.equals("encrypt")){
     //Send encrypted string to terminal
     Log.print(Encrypt.eString(args[0]));
     
     //Display results
     msg("Printed result to console");
     command("_");
   }
   
   //Action for "Mod" command
   else if(cmd.equals("mod")){
     //Change nada
     if(! loggedIn){
       msg("Not logged into any user");
       command("");
       return;
     }
     
     //Change username
     if(args[0].equals("name")){
       users.getL().changeName(args[1]);
       msg("Changed username to "+args[1]);
       command("_");
     }
     
     //Change user song
     else if(args[0].equals("song")){
       if(Integer.parseInt(args[1])>4){
         msg("Song value must be at most "+homeSong.length);
         command("_");
         return;
       }
       homeSong[users.loggedInSng()].pause();
       users.getL().changeSong(Integer.parseInt(args[1])-1);
       homeSong[users.loggedInSng()].rewind();
       homeSong[users.loggedInSng()].play();

       msg("Changed song to "+args[1]);
       command("_");
     }
     
     //Change nada
     else{
       msg("Unknown Modification");
       command("_");
     }
   }
   
   //Action for any other command
   else{
     msg("Command unknown");
     command("_");
   }
  }
  
  //Scroll to clear console
  void clear(){
    scroll = lineLoc;
    lineSee = 0;
  }

  //Return domain format
  String dF(String in){
    return "$"+in+"$ > ";
  }

  //Delete last char from current line
  void delete(){
    if(line.length()>1){
      line = line.substring(0,line.length()-2)+"_";
    }
    lines.set(lineLoc-1,line);
  }
  //Delete underscore from current line
  void deleteU(){
    line = line.substring(0,line.indexOf("_"));
    lines.set(lineLoc-1,line);
  }
  //Remove escape chars from current line
  void remEsc(){
    int zai = line.indexOf("\n");
    try{
      line = line.substring(0,zai-1) + line.substring(zai+2);
    }catch(IndexOutOfBoundsException e){
      return;
    }
  }
  
  //Scroll up or down
  void scroll(float dir){
    if(dir == 1.0){
      if(scroll<lineCount){
        scroll++;
      }
    }
    else{
      if(scroll>0){
        scroll--;
      }
    }
  }
  
  //Play a home song
  void playSong(String val){
    if(Integer.parseInt(val)>homeSong.length){
      msg("We don't have that many songs");
      command("_");
      return;
    }
    if(loggedIn){
      homeSong[users.loggedInSng()].pause();
      homeSong[Integer.parseInt(val)-1].rewind();
      homeSong[Integer.parseInt(val)-1].play();
    }
    else{
      for(AudioPlayer a:homeSong){
        a.pause();
        a.rewind();
      }
      track[songPlaying].pause();
      homeSong[Integer.parseInt(val)-1].play();
    }
  }
}
