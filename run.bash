#!/bin/bash
loc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
myos=`uname`
cd $loc

if [ -d $loc/build ]; then
  rm -r build
fi  

if [[ "$myos" == 'Darwin' ]]; then
  if [ -f ~/processing-java ]; then
    cd ~/
    ./processing-java --sketch=$loc --output=$loc/build --run
  else
    processing-java --sketch=$loc --output=$loc/build --run
  fi
else
  cd C:/processing-3.0.2
  chmod +x processing-java.exe
  ./processing-java.exe --sketch=$loc --output=$loc/build --run
fi
