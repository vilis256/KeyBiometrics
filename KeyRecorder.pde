public class KeyRecorder {
  private long holdKey;
  private long between;
  private char[] keys;
  private long hTime;
  private long bTime;
  private boolean hRec = false;
  private boolean bRec = false;

  public KeyRecorder() {
    holdKey = 0;
    between = 0;
    keys = new char[2];
  }

  public void startHRec(char key1) {
    hTime = millis();
    keys[0] = key1;
    hRec = true;
  }
  public void stopHRec() {
    holdKey = millis()-hTime;
    hRec = false;
  }

  public void startBRec() {
    bTime = millis();
    bRec = true;
  }
  public void stopBRec(char key2) {
    between = millis()-bTime;
    keys[1] = key2;
    bRec = false;
  }

  public boolean hRec() {
    return hRec;
  }
  public boolean bRec() {
    return bRec;
  }

  public KeyMotion finish() {
    return new KeyMotion(keys[0], keys[1], holdKey, between);
  }

  public void reset() {
    holdKey =0;
    between =0;
    hTime = 0;
    bTime = 0;
    hRec = false;
    bRec = false;
    keys[0] = ' ';
    keys[1] = ' ';
  }
}