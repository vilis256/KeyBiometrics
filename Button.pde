public class Button {
  private String display;
  private int xPos, yPos;
  private boolean active;
  private int wid, hit;
  private boolean clicked;
  private color c;
  private color c2;
  private int fnt;

  public Button(String d, int a, int b, int fnt1, color c1) {
    fnt=fnt1;
    display = d;
    xPos = a;
    yPos = b;
    active = false;
    hit = fnt+5;
    wid = display.length()*(fnt/2)+10;
    clicked = false;
    c=c1;
    c2 = c1;
  }
   public Button(String d, int a, int b, color c1) {
    fnt=width/30;
    display = d;
    xPos = a;
    yPos = b;
    active = false;
    hit = fnt+5;
    wid = display.length()*(fnt/2)+10;
    clicked = false;
    c=c1;
    c2 = c1;
  }

  public void show() {
    stroke(c);
    fill(c,100);
    rect(xPos-6, yPos, wid+6, hit);
    fill(255);
    textAlign(LEFT, TOP);
    textSize(fnt);
    text(display, xPos+3, yPos+3);
  }

  public void activate() {
    active = true;
  }
  public void deactivate() {
    active = false;

    //clicked = false;
  }

  public void click(int x, int y) {

    if (active && x>=xPos && x<=(xPos+wid) && y>=yPos & y <=(yPos+hit)) {

      clicked = true;
      c = invC(c);
      show();
    } else {
      clicked = false;
    }
  }

  public boolean clicked() {
    if (clicked) {
      clicked = false;
      c = c2;
      return true;
    }
    return false;
  }

  public boolean active(){
    return active;
  }

  public void forceClick(){
    clicked = true;
  }
}
