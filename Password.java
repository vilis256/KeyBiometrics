import java.util.*;
import java.io.*;

public class Password {
  KeyMap db;
  private long tol;
  private int acceptableErrors;
  private String trace = "";

  //New user constructor
  public Password(KeyMap k) {
    tol=80;
    db=k;
    acceptableErrors = 6;
  }

  //Uploaded user constructor
  public Password(KeyMap k,long t,int a) {
    tol=t;
    db=k;
    acceptableErrors = a;
  }

  //Check if typing patterns are within acceptable range
  public boolean verify(KeyMap p) {
    if (db.compare(p, tol)<=acceptableErrors) {
      if (acceptableErrors>=3) {
        acceptableErrors--;
        tol-=5;
      }
      return true;
    } else {
      return false;
    }

  }

  //Update typing pattern
  public void learn(KeyMap other) {
    db.learn(other);
  }

  //Return password data
  public String print() {
    return tol+":"+acceptableErrors+"\n"+Encrypt.encrypt(db.print());
  }

  //Calculate average of longs
  public long mean(long... a) {
    long sum = 0;
    for (long i : a) {
      sum+=i;
    }
    return sum/a.length;
  }

  //Get methods
  public KeyMap getPass() {
    return db;
  }

  public String getTrace() {
    return trace;
  }

  public long getTol() {
    return tol;
  }
}
