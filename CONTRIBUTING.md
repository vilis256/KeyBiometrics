**CONTRIBUTORS:**

*1. Developers:*
  1. Jake Trimble  
  2. Aidan Barbieux  

*2. Initial team:*   
  1. Gus Siegel  
  2. Byron Osborne  

*3. Beta Testers:*    
  1. Jackson Weidmann  

**TASKS:**

1. Graphics:  
  1. Aidan Barbieux  
  2. Jake Trimble    

2. Algorithims:    
  1. Jake Trimble  

3. Interface:
  1. Aidan Barbieux  
  2. Jake Trimble  
  3. *Credit to Byron Osborne and Gus Siegel for initial design*  
