import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import ddf.minim.*; 
import java.util.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class KeyBiometrics extends PApplet {



UserDB users;
LoginMenu login;
LoginMenu newUser;
boolean loggedIn = false;
boolean mainMenu = true;
boolean loginMenu = false;
boolean nUserMenu = false;
KeyMap signature = new KeyMap();
KeyMap[] learnPattern = new KeyMap[6];
KeyRecorder loginPass = new KeyRecorder();
KeyRecorder[] nuLearn = new KeyRecorder[8];
ArrayList<Button> theButtons = new ArrayList<Button>();
ArrayList<Button> lesButtons=new ArrayList<Button>();
PImage back, failBack, home, instruct;
PFont futr;
Minim minim;
AudioPlayer[] track;
AudioPlayer[] homeSong;
int songPlaying;
SLog log;
boolean mute = true;

float px, py;


public void setup() {

  
  //size(800,800);
  px=(width/1920.0f)*1260.0f;
  py=(height/1080.0f)*165.0f;
  futr=createFont("DS-DIGI.TTF", width/30);

  back=loadImage("GUI.png");
  failBack=loadImage("GUI_F.png");
  home = loadImage("HomePage.png");
  instruct = loadImage("Instruct.png");
  back.resize(width, height);
  failBack.resize(width, height);

  log = new SLog(40,13,px,py,5);

  log.domain("System");
  log.command("Beginning startup...");
  log.command("Loading Library KeyBiometrics...");
  log.command("Complete! | Running Setup...");

  image(back, 0, 0, width, height);
  log.show();

  log.subCmd("Fonts and Images loaded...");

  image(back, 0, 0, width, height);
  log.show();

  minim = new Minim(this);
  track = new AudioPlayer[9];
  track[0] = minim.loadFile("Track.mp3");
  track[1] = minim.loadFile("Track2.mp3");
  track[2] = minim.loadFile("Track3.mp3");
  track[3] = minim.loadFile("Track4.mp3");
  track[4] = minim.loadFile("Track5.wav");
  track[5] = minim.loadFile("Track6.wav");
  track[6] = minim.loadFile("Track7.wav");
  track[7] = minim.loadFile("Track8.wav");
  track[8] = minim.loadFile("TrackX.mp3");

  homeSong = new AudioPlayer[4];
  homeSong[0] = minim.loadFile("Home1.mp3");
  homeSong[1] = minim.loadFile("Home2.mp3");
  homeSong[2] = minim.loadFile("Home3.mp3");
  homeSong[3] = minim.loadFile("Home4.mp3");

  log.subCmd("Sound Files loaded...");
  songPlaying = 0;

  image(back, 0, 0, width, height);
  log.show();

  login = new LoginMenu("Login", false, width, height);
  newUser = new LoginMenu("Create a New User", true, width, height);

  try {
    users = new UserDB(dataPath(""));

    log.subCmd("User Data loaded...");
    image(back, 0, 0, width, height);
    log.show();
  }
  catch(IOException e) {
    Log.printErr("BAD IOEXCEPTION!!!!", e);

    log.subCmd("User Data load failed!...");
    image(back, 0, 0, width, height);
    log.show();
  }

  for (int i=0; i<6; i++) {
    nuLearn[i] = new KeyRecorder();
    learnPattern[i] = new KeyMap();
  }

  theButtons.add(new Button("Login", (int)(width-width/(2.9f))-10, 10, 0xff07CFF0));
  theButtons.add(new Button("Create User", (int)(width-width/(4)), 10, 0xff07CFF0));
  theButtons.add(new Button("Quit", 10, 10, 0xff07CFF0));
  theButtons.add(new Button("Reset", width/2-100, height/2, color(255, 0, 0)));
  theButtons.add(new Button("Logout", width-(width/33)*7,10,0xff07CFF0));
  theButtons.add(new Button("Cancel", width-(width/33)*7,10,color(255,0,0)));
  if (! mute){
    track[songPlaying].play();
  }

  if(users.ad()){
    log.msg("Setup Complete!");
    log.domain("Admin");
    log.command("_");
  }
  else{
    log.domain("System");
    log.msg("Setup Complete!");
    log.command("Waiting for user input...");
  }

  image(back, 0, 0, width, height);
  log.show();

  theButtons.get(0).activate();
  theButtons.get(1).activate();
}

public void draw(){
  if (! loggedIn) {
    if (! track[songPlaying].isPlaying() && ! mute) {
      Log.print("Next Song");
      track[songPlaying].pause();
      track[songPlaying].rewind();
      songPlaying ++;

      if (songPlaying > 7) {
        songPlaying = 0;
      }

      track[songPlaying].play();
    }

    mainMenu();
  } else {
    userHome();
    theButtons.get(3).deactivate();
  }

  if (loginMenu) {
    login.display();
    theButtons.get(3).activate();
    theButtons.get(3).show();
    theButtons.get(5).show();
    if(theButtons.get(5).clicked()){
      loginPass.reset();
      login.resetAll();
      theButtons.get(0).activate();
      theButtons.get(1).activate();
      loginMenu = false;
      theButtons.get(5).deactivate();
    }
  }


  if (nUserMenu) {

    newUser.display();
    theButtons.get(5).show();
    if(theButtons.get(5).clicked()){
      loginPass.reset();
      login.resetAll();
      theButtons.get(0).activate();
      theButtons.get(1).activate();
      theButtons.get(5).deactivate();
      for(KeyRecorder i:nuLearn){
        try{
          i.reset();
        }catch(NullPointerException e){
          //ignore
        }
      }
      nUserMenu = false;
    }
    for (Button i : lesButtons) {
      i.activate();
      i.show();
    }
    for (int i=0; i<lesButtons.size(); i++) {
      if (lesButtons.get(i).clicked()) {
        newUser.clearPass(i);
        if (nuLearn[i]!=null) {
          nuLearn[i].reset();
          learnPattern[i].clear();
        }
      }
    }
  }


  theButtons.get(2).activate();
  theButtons.get(2).show();
}

public void logoutMain(){
  homeSong[users.loggedInSng()].pause();
  homeSong[users.loggedInSng()].rewind();

  try{
    users.close();
  }catch(IOException e){
    //Do nada
  }
  users.logoutCurrent();

  newUser.resetAll();
  login.resetAll();

  signature.clear();
  loginPass.reset();

  theButtons.get(4).deactivate();
  theButtons.get(0).activate();
  theButtons.get(1).activate();

  for(KeyRecorder i:nuLearn){
    try{
      i.reset();
    }catch(NullPointerException e){
      //ignore
    }
  }
  for(KeyMap j:learnPattern){
    try{
      j.clear();
    }catch(NullPointerException e){
      //ignore
    }
  }

  loggedIn = false;
  mainMenu = true;

  log.domain("User");
  log.command("sudo -logout");
  log.msg("Logged out of user "+users.loggedInNm());
  log.domain("/");
  log.command("cd System");
  log.domain("System");
  log.command("Waiting for user input...");
}
public void quit() {
  if (loggedIn) {
    try {
      users.close();
    }
    catch(IOException e) {
      Log.printErr("!@#$", e);
    }
  }
  exit();
}
public class Button {
  private String display;
  private int xPos, yPos;
  private boolean active;
  private int wid, hit;
  private boolean clicked;
  private int c;
  private int c2;
  private int fnt;

  public Button(String d, int a, int b, int fnt1, int c1) {
    fnt=fnt1;
    display = d;
    xPos = a;
    yPos = b;
    active = false;
    hit = fnt+5;
    wid = display.length()*(fnt/2)+10;
    clicked = false;
    c=c1;
    c2 = c1;
  }
   public Button(String d, int a, int b, int c1) {
    fnt=width/30;
    display = d;
    xPos = a;
    yPos = b;
    active = false;
    hit = fnt+5;
    wid = display.length()*(fnt/2)+10;
    clicked = false;
    c=c1;
    c2 = c1;
  }

  public void show() {
    stroke(c);
    fill(c,100);
    rect(xPos-6, yPos, wid+6, hit);
    fill(255);
    textAlign(LEFT, TOP);
    textSize(fnt);
    text(display, xPos+3, yPos+3);
  }

  public void activate() {
    active = true;
  }
  public void deactivate() {
    active = false;

    //clicked = false;
  }

  public void click(int x, int y) {

    if (active && x>=xPos && x<=(xPos+wid) && y>=yPos & y <=(yPos+hit)) {

      clicked = true;
      c = invC(c);
      show();
    } else {
      clicked = false;
    }
  }

  public boolean clicked() {
    if (clicked) {
      clicked = false;
      c = c2;
      return true;
    }
    return false;
  }

  public boolean active(){
    return active;
  }

  public void forceClick(){
    clicked = true;
  }
}
class Field {
  String name;
  private boolean editable;
  private boolean canClick;
  private boolean canEdit;
  private float x, y, w, h;
  private String contents;
  private String tempContents;
  private int border;

  Field(String n, boolean e, int a, int b, int c, int d) {
    name = n;
    editable = e;
    x = a;
    y = b;
    w = c;
    h = d;
    contents = "";
    tempContents = "";
    canClick = false;
    canEdit = false;
    border = color(0);
  }

  Field(String n, boolean e, int a, int b, int c, int d, String co) {
    name = n;
    editable = e;
    x = a;
    y = b;
    w = c;
    h = d;
    contents = "";
    tempContents = co;
    canClick = false;
    canEdit = false;
    border = color(0);
  }
  public float[] getDim(){
    float[] a=new float[4];
    a[0]=x;
    a[1]=y;
    a[2]=w;
    a[3]=h;
    return a;
  }

  public void show() {
    fill(0);
    textSize(20);

    if (name != "") {
      //fill(255,0,0);
      text(name + ":", x, y-30);
      //fill(0);
    }
    if (! editable) {
      text(contents, x, y-30);
      fill(0);
    } else {
      canClick = true;
      fill(255);
      strokeWeight(2);
      stroke(border);
      rect(x, y, w, h);
      strokeWeight(1);

      fill(100,100,150);
      text(tempContents,x+5,y+3);

      fill(0);
      text(contents, x+5, y+3);
      stroke(0);
    }
  }

  public void hide() {
    canClick = false;
    canEdit = false;
  }

  public void setEdit(boolean a) {
    if (canClick) {
      canEdit = a;

      if (canEdit) {
        border = color(200,20,20);


      } else {
        border = color(0);
      }
    }
  }

  public boolean edit() {
    return canEdit;
  }

  public void add(char let) {
    contents += let;
  }
  public void del() {
    if (contents.length()>0)
      contents = contents.substring(0, contents.length()-1);
  }

  public boolean inside(float xI, float yI) {
    float toX = xI-x;
    float toY = yI-y;
    if (toX < 0 || toY <0 || toX > w || toY > h+10) {
      return false;
    }

    return true;
  }

  public void translator(float xT, float yT) {
    x += xT;
    y += yT;
  }

  public String getContents() {
    return contents;
  }

  public void erase(){
    contents = "";
  }

  //boolean stCont() {
  //  return tempContents;
  //}
}


public void mouseClicked() {
  clickAll(mouseX, mouseY);
  if (!loggedIn) {
    if (login.active()) {
      login.click(mouseX, mouseY);
    }
    if (newUser.active()) {
      newUser.click(mouseX, mouseY);
    }
  }
}

public void mouseWheel(MouseEvent event) {
  float a = event.getCount();
  log.scroll(a);
}

public void keyTyped() {
  if (key == ENTER || key == RETURN) {
    if(mainMenu && !loginMenu && !nUserMenu && users.ad()){
      log.process();
    }
    return;
  }

  if(mainMenu && !loginMenu && !nUserMenu && users.ad()){
    if(key != DELETE && key != BACKSPACE){
      log.type(key);
    }
    else{
      log.delete();
    }
    
  }

  if (login.active()) {
    if (key != DELETE && key != BACKSPACE) {
      login.type(key);
    } else {
      login.del();
    }

    if (login.complete()) {
      login.hide();
      loginMenu = false;
      loggedIn = true;
      try {
        users.check(login.user(), signature);
        if (users.getErr().equals("Nil") && ! mute) {
          track[songPlaying].pause();
          homeSong[users.loggedInSng()].rewind();
          homeSong[users.loggedInSng()].play();
        } else if (! mute) {
          track[songPlaying].pause();
          track[8].play();
        }
        log.clear();
        log.domain("System");
        log.msg("Login Success! Welcome " + users.loggedInNm());
        log.command("cd User");
        log.domain("User");
        log.command("_");

        if (users.loggedInNm().equals("Joe Smith")) {
          log.clear();
          log.domain("Error");
          log.msg(users.getErr());
          log.domain("System");
          log.command("_");
        }
      }
      catch(IOException e) {
        Log.printErr(e, 82);
      }
    }
  } else if (newUser.active()) {
    if (key != DELETE && key != BACKSPACE) {
      newUser.type(key);
    } else {
      newUser.del();
    }

    if (newUser.allComplete()) {
      newUser.hide();
      nUserMenu = false;
      loggedIn = true;
      users.newUser(newUser.user(), learnPattern);
      log.clear();
      if (users.getErr().equals("Nil")) {
        log.domain("System");
        log.msg("Succesfully created user! Welcome " + users.loggedInNm());
        log.command("cd User");
        log.domain("User");
        log.command("_");
      } else {
        log.domain("Error");
        log.msg(users.getErr());
      }
    }
  }
}

public void mainMenu() {
  log.domain("System");

  if (users.first()) {
    image(instruct, 0, 0, width, height);
  } else {
    image(back, 0, 0, width, height);
  }
  if (!loginMenu && !nUserMenu) {
    if (! users.first()) {
      theButtons.get(0).show();
    }
    theButtons.get(1).show();
  }

  log.show();

  if (theButtons.get(2).clicked()) {
    quit();
  }
  if (theButtons.get(0).clicked()) {
    log.command("Running Login Menu...");
    loginMenu = true;
    theButtons.get(0).deactivate();
    theButtons.get(1).deactivate();
    theButtons.get(5).activate();
  }
  if (theButtons.get(1).clicked()) {
    log.command("Running Create User...");
    nUserMenu = true;
    theButtons.get(0).deactivate();
    theButtons.get(1).deactivate();
    theButtons.get(5).activate();
  }
  if (loginMenu) {
    if (theButtons.get(3).clicked()) {
      loginPass.reset();
      signature.clear();
      login.clearPass();
    }
  }
}

public void userHome() {
  log.domain("User");
  //Background
  image(home, 0, 0, width, height);
  fill(255, 0, 0);
  textSize(30);

  //Log display
  if (! users.loggedInNm().equals("Joe Smith")) {
    text(users.loggedInNm(), width/6+20, 140);

    log.show();
  } else {

    //Fail background
    //fill(15, 110, 125, 100);
    image(failBack, 0, 0);
    log.changeC(0);
    //rect(0, 0, width, height);

    log.show();
  }

  //Click the quit button
  if (theButtons.get(2).clicked()) {
    quit();
  }

  //Hide everything but quit button
  theButtons.get(0).deactivate();
  theButtons.get(1).deactivate();
  theButtons.get(5).deactivate();
  theButtons.get(2).show();

  //Show Logout button
  theButtons.get(4).activate();
  theButtons.get(4).show();

  //Logout Button Function
  if (theButtons.get(4).clicked()) {
    logoutMain();
    log.changeC(color(45, 200, 210));
  }
}

public void keyPressed() {
  if (loginMenu) {
    if (login.editingPass()) {
      if (loginPass.bRec()) {
        if (key == DELETE || key == BACKSPACE) {
          loginPass.reset();
        } else {
          loginPass.stopBRec(key);
          signature.addThis(loginPass.finish());
          loginPass.reset();
          loginPass.startHRec(key);
        }
      } else if (! loginPass.hRec()) {
        loginPass.startHRec(key);
      }
    }
  }

  if (nUserMenu) {
    if (newUser.editingLearn() > -1) {
      int ind = newUser.editingLearn();
      if (nuLearn[ind].bRec()) {
        if (key == DELETE || key == BACKSPACE) {
          nuLearn[ind].reset();
        } else {
          nuLearn[ind].stopBRec(key);
          learnPattern[ind].addThis(nuLearn[ind].finish());
          nuLearn[ind].reset();
          nuLearn[ind].startHRec(key);
        }
      } else if (! nuLearn[ind].hRec()) {
        nuLearn[ind].startHRec(key);
      }
    }
  }
}

public void keyReleased() {
  if (loginMenu) {
    if (login.editingPass()) {
      loginPass.stopHRec();
      loginPass.startBRec();
    }
  }

  if (nUserMenu) {
    int ind = newUser.editingLearn();
    if (newUser.editingLearn()>-1) {
      nuLearn[ind].stopHRec();
      nuLearn[ind].startBRec();
    }
  }
}

public void clickAll(int x, int y) {
  for (Button i : theButtons) {
    i.click(x, y);
  }
  for (Button i : lesButtons) {
    i.click(x, y);
  }
}

public int invC(int a) {
  float r=red(a), g=green(a), b=blue(a);
  int ret=color(255-r, 255-g, 255-b);
  return ret;
}
public class KeyRecorder {
  private long holdKey;
  private long between;
  private char[] keys;
  private long hTime;
  private long bTime;
  private boolean hRec = false;
  private boolean bRec = false;

  public KeyRecorder() {
    holdKey = 0;
    between = 0;
    keys = new char[2];
  }

  public void startHRec(char key1) {
    hTime = millis();
    keys[0] = key1;
    hRec = true;
  }
  public void stopHRec() {
    holdKey = millis()-hTime;
    hRec = false;
  }

  public void startBRec() {
    bTime = millis();
    bRec = true;
  }
  public void stopBRec(char key2) {
    between = millis()-bTime;
    keys[1] = key2;
    bRec = false;
  }

  public boolean hRec() {
    return hRec;
  }
  public boolean bRec() {
    return bRec;
  }

  public KeyMotion finish() {
    return new KeyMotion(keys[0], keys[1], holdKey, between);
  }

  public void reset() {
    holdKey =0;
    between =0;
    hTime = 0;
    bTime = 0;
    hRec = false;
    bRec = false;
    keys[0] = ' ';
    keys[1] = ' ';
  }
}
class Menu {
  private String title;
  private ArrayList<Field> fields;
  private boolean active;
  private int w, h;
  Menu(String t, ArrayList<Field> f) {
    title = t;
    fields = f;
    active = false;
  }
  Menu(String t) {
    title = t;
    fields = new ArrayList<Field>();
    active = false;
  }
  public void siz(int a, int b) {
    w=a;
    h=b;
  }
  public void display() {
    active = true;

    fill(170, 150, 170, 150);
    rect(0, 0, width, height);

    fill(180, 150);
    rectMode(CENTER);
    rect(width/2, height/2, 2*width/3, 2*height/3);
    rectMode(CORNER);
    textSize(40);
    fill(0);
    int titPos = (width/2)-((title.length() * 30)/2);
    text(title, titPos, 50);


    for (Field i : fields) {
      i.show();
    }
  }

  public boolean active() {
    return active;
  }

  public void hide() {
    active = false;

    for (Field i : fields) {
      i.hide();
    }
  }

  public void click(float a, float b) {
    for (Field i : fields) {
      if (i.inside(a, b)) {
        i.setEdit(true);
        for (Field j : fields) {
          if (j != i) {
            j.setEdit(false);
          }
        }
      }
    }
  }

  public void type(char let) {
    for (Field i : fields) {
      if (i.edit()) {
        i.add(let);
      }
    }
  }

  public void del() {
    for (Field i : fields) {
      if (i.edit()) {
        i.del();
      }
    }
  }

  public void addField(Field e) {
    fields.add(e);
  }

  public boolean editingPass() {
    for (Field i : fields) {
      if (i.name.equals("Type the phrase in the box") && i.edit()) {
        return true;
      }
    }
    return false;
  }

  public int editingLearn() {
    for (int i=0; i<fields.size(); i++) {
      if (fields.get(i).name.equals("Type the phrases in the boxes") || fields.get(i).name.equals("") && fields.get(i).edit()) {
        return i-1;
      }
    }
    return -1;
  }

  public ArrayList<Field> fields() {
    return fields;
  }

  public void resetAll(){
    for(Field i:fields){
      i.erase();
    }
  }
}

class LoginMenu extends Menu {
  private String[] phrases = {"the appsword is password", "the end is nigh", "hi my name is sam", "log in to login", "as seen on tv", "ask your doctor", "quidditch"};
  private final int ran = (int)random(phrases.length);
  private String phrase = phrases[ran];
  private int w, h;
  LoginMenu(String t, boolean s, int qx, int qy) {
    super(t);
    w=qx;
    h=qy;
    int xf=width/6+10;
    int yf=height/4+10;
    int wid=2*width/5 ;
    int hit=height/33;
    Field a = new Field("Username", true, xf, yf-40, wid, hit);
    addField(a);

    if (s) {
      Field b = new Field("Type the phrases in the boxes", true, xf, yf+40, wid, hit, phrases[0]);
      Field c = new Field("", true, xf, yf+80, wid, hit, phrases[1]);
      Field d = new Field("", true, xf, yf+120, wid, hit, phrases[2]);
      Field e = new Field("", true, xf, yf+160, wid, hit, phrases[3]);
      Field f = new Field("", true, xf, yf+200, wid, hit, phrases[4]);
      Field g = new Field("", true, xf, yf+240, wid, hit, phrases[5]);
      Field k = new Field("", true, xf, yf+280, wid, hit, phrases[6]);

      addField(b);
      addField(c);
      addField(d);
      addField(e);
      addField(f);
      addField(g);
      addField(k);
      ArrayList<Field> fields=fields();
      Field tempr=fields.remove(0);
      for(Field i:fields){
        float[] te=i.getDim();
        lesButtons.add(new Button("RESET",(int)te[0]+wid+20,(int)te[1]+5,12,color(255,0,0)));
      }
      fields.add(0,tempr);

    } else {
      Field b = new Field("Type the phrase in the box", true, xf, yf+40, 500, 25, phrase);
      addField(b);
    }
  }

  public String user() {
    return fields().get(0).getContents();
  }

  public void siz(int a, int b) {
    w=a;
    h=b;
  }

  public boolean complete() {
    return fields().get(1).getContents().equalsIgnoreCase(phrase);
  }

  public boolean complete(int fN) {
    return fields().get(fN).getContents().equalsIgnoreCase(phrases[fN-1]);
  }

  public boolean allComplete() {
    for (int i=1; i<8; i++) {
      if (! complete(i)) {
        return false;
      }
    }

    return true;
  }

  public void clearPass() {
    fields().get(1).erase();
  }
  public void clearPass(int a) {
    fields().get(a+1).erase();
  }
}
class SLog{
  private String domain;
  private String line;

  private ArrayList<String> lines;
  private ArrayList<String> domains;

  private int lineCount;
  private int lineLoc;
  private int lineSee;

  private final int maxCharCount;
  private final int maxLineCount;

  private float xLoc;
  private float yLoc;
  private int theSize;

  private int scroll;
  private int dec;

  private int c;

  //Constructor
  public SLog(int w,int h,float x,float y,int tS){
    maxCharCount = w;
    maxLineCount = h;
    xLoc = x;
    yLoc = y;
    theSize = tS;
    domain = "";
    lineLoc = 0;
    lineSee = 0;
    line = "";
    lineCount = 0;
    lines = new ArrayList<String>();
    domains = new ArrayList<String>();
    scroll = 0;
    dec = -1;
    c=color(45, 200, 210);
  }

  //Message display modes
  public void command(String txt){
      line = txt;

      // if(line.length()+domain.length() > maxCharCount){
      //   line = line.substring(0,46) + "\n\t" + line.substring(46);
      // }

      lines.add(line);
      domains.add(dF(domain));
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }
  public void msg(String txt){
      line = txt;

      // if(line.length()+domain.length() > maxCharCount){
      //   line = line.substring(0,46) + "\n\t" + line.substring(46);
      // }

      lines.add(line);
      domains.add(dF(domain+"Log"));
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }
  public void subCmd(String txt){
      line = txt;

      // if(line.length()+domain.length() > maxCharCount){
      //   line = line.substring(0,46) + "\n\t" + line.substring(46);
      // }

      lines.add(line);
      domains.add("\t> ");
      lineCount++;
      lineLoc++;
      lineSee++;

      if(lineSee>maxLineCount){
        scroll++;
        lineSee--;
      }
  }

  //Set domain
  public void domain(String d){
    domain = d;
  }

  //Change text color
  public void changeC(int c1){
    c=c1;
  }

  //Display Console
  public void show(){
    textFont(futr);
    textSize(width/120);

    fill(c);
    for(int i=scroll;i<lines.size();i++){
      if(! (((yLoc-10)+((i-scroll)*(width/120)+10)) > ((yLoc-10)+(maxLineCount)*(width/120)+10))){
        text(domains.get(i) + lines.get(i), xLoc, ((yLoc-10)+((i-scroll)*(width/120)+10)));
      }
    }
  }

  //Add letters to current line
  public void type(char let){
    if(lineCount-scroll > maxLineCount){
      scroll = lineCount-2;
    }
    deleteU();

    line += let;
    line += "_";
    if(line.length()+domain.length() > maxCharCount){
      remEsc();
      line = line.substring(0,40) + "\n\t" + line.substring(40);
    }
    lines.set(lineLoc-1,line);
    //Log.print(""+lineLoc);
  }

  //Do something based on an inputed command
  public void process(){

   //Cut up parts of a command
   String[] parts = line.substring(0,line.length()-1).split(" ");
   String cmd = parts[0];
   Log.print("Running command"+cmd);
   String[] args;

   if(parts.length>1){
     args = Arrays.copyOfRange(parts,1,parts.length);;
   }else{
     args = null;
   }

   //Action for "Login" command
   if(cmd.equals("login")){
     Log.print("Running login cmd...");

     //Attempt to login
     try{
       users.loginTo(args[0]);
     }catch(NullPointerException e){
       String t = domain;
       domain("Error");
       msg("User does not exist");
       domain(t);
       command("_");
       return;
     }

     loggedIn = true;

     //Play the correct song
     if (users.getErr().equals("Nil") && ! mute) {
       track[songPlaying].pause();
       homeSong[users.loggedInSng()].rewind();
       homeSong[users.loggedInSng()].play();
     } else if (! mute) {
       track[songPlaying].pause();
       track[8].play();
     }

     //Display result
     clear();
     domain("System");
     msg("Login Success! Welcome " + users.loggedInNm());
     command("cd User");

     if(users.getErr().equals("Nil")){
       domain("User");
       command("_");
     }
     else{
       domain("Error");
       msg(users.getErr());
     }

     if (users.loggedInNm().equals("Joe Smith")) {
       clear();
       domain("Error");
       msg(users.getErr());
       domain("System");
       command("_");
     }
   }

   //Action for "Click" command
   else if(cmd.equals("click")){
     //Click a button
     theButtons.get(Integer.parseInt(args[0])).forceClick();
   }

   //Action for "Logout" command
   else if(cmd.equals("logout")){
     //Switch songs
     if(loggedIn){
       if(! mute){
         homeSong[users.loggedInSng()].pause();
         track[songPlaying].play();
       }
       //Logout
       users.logoutCurrent();
     }
     else{
       msg("Not logged in to any user");
       command("_");
       return;
     }

     //Display Results
     loggedIn=false;
     clear();
     domain("User");
     msg("Logging out...");
     domain("System");
     command("_");
   }

   //Action for "Play" command
   else if(cmd.equals("play")){

     //Play a homeSong
     if(args[0].equals("-s")){
       playSong(args[1]);
       return;
     }

     //To high of an index
     if(Integer.parseInt(args[0])>track.length){
       msg("We don't have that many songs");
       command("_");
       return;
     }

     //Stop current song and play new one
     if(loggedIn){
       homeSong[users.loggedInSng()].pause();
       track[Integer.parseInt(args[0])-1].rewind();
       track[Integer.parseInt(args[0])-1].play();
     }
     else{
       track[songPlaying].pause();
       songPlaying = Integer.parseInt(args[0])-1;
       Log.print(songPlaying+"");
       track[songPlaying].play();
     }

     //Display results
     String tem = domain;
     domain("System");
     msg("Playing song #"+args[0]);
     domain(tem);
     command("_");
   }

   //Action for "Quit" command
   else if(cmd.equals("quit")){
     //Display process
     msg("Exiting program...");
     show();
     //Quit
     quit();
   }

   //Action for "Encrypt" command
   else if(cmd.equals("encrypt")){
     //Send encrypted string to terminal
     Log.print(Encrypt.eString(args[0]));

     //Display results
     msg("Printed result to console");
     command("_");
   }

   //Action for "Mod" command
   else if(cmd.equals("mod")){
     //Change nada
     if(! loggedIn){
       msg("Not logged into any user");
       command("");
       return;
     }

     //Change username
     if(args[0].equals("name")){
       users.getL().changeName(args[1]);
       msg("Changed username to "+args[1]);
       command("_");
     }

     //Change user song
     else if(args[0].equals("song")){
       if(Integer.parseInt(args[1])>4){
         msg("Song value must be at most "+homeSong.length);
         command("_");
         return;
       }
       homeSong[users.loggedInSng()].pause();
       users.getL().changeSong(Integer.parseInt(args[1])-1);
       homeSong[users.loggedInSng()].rewind();
       homeSong[users.loggedInSng()].play();

       msg("Changed song to "+args[1]);
       command("_");
     }

     //Change nada
     else{
       msg("Unknown Modification");
       command("_");
     }
   }

   //Action for any other command
   else{
     msg("Command unknown");
     command("_");
   }
  }

  //Scroll to clear console
  public void clear(){
    scroll = lineLoc;
    lineSee = 0;
  }

  //Return domain format
  public String dF(String in){
    return "$"+in+"$ > ";
  }

  //Delete last char from current line
  public void delete(){
    if(line.length()>1){
      line = line.substring(0,line.length()-2)+"_";
    }
    lines.set(lineLoc-1,line);
  }
  //Delete underscore from current line
  public void deleteU(){
    if(! line.contains("_")){
      return;
    }
    line = line.substring(0,line.indexOf("_"));
    lines.set(lineLoc-1,line);
  }
  //Remove escape chars from current line
  public void remEsc(){
    int zai = line.indexOf("\n");
    try{
      line = line.substring(0,zai-1) + line.substring(zai+2);
    }catch(IndexOutOfBoundsException e){
      return;
    }
  }

  //Scroll up or down
  public void scroll(float dir){
    if(dir == 1.0f){
      if(scroll<lineCount){
        scroll++;
      }
    }
    else{
      if(scroll>0){
        scroll--;
      }
    }
  }

  //Play a home song
  public void playSong(String val){
    if(Integer.parseInt(val)>homeSong.length){
      msg("We don't have that many songs");
      command("_");
      return;
    }
    if(loggedIn){
      homeSong[users.loggedInSng()].pause();
      homeSong[Integer.parseInt(val)-1].rewind();
      homeSong[Integer.parseInt(val)-1].play();
    }
    else{
      for(AudioPlayer a:homeSong){
        a.pause();
        a.rewind();
      }
      track[songPlaying].pause();
      homeSong[Integer.parseInt(val)-1].play();
    }
  }
}
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "KeyBiometrics" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
