import java.io.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class UserDB {
  private ArrayList<User> users;
  private String loc;
  private String errorMessage;
  boolean firstTime;
  boolean isAdmin;

  //Constructor
  public UserDB(String l) throws IOException {
    //Initialize user list
    users = new ArrayList<User>();

    //Upload list of users
    loc = l;
    String list = "";
    FileInputStream uses;

    try{
      uses = new FileInputStream(loc+"/Users.txt");
    }catch(IOException e){
      uses = null;
    }

    if(uses != null){
      firstTime = false;
      while (uses.available() > 0) {
        char temp = (char)uses.read();

        if(temp != '\r'){
          list += temp;
        }
      }
      uses.close();

      String[] userList = list.split("\n");

      if(Encrypt.bannanas(userList[0]).equals("isAdmin")){
        isAdmin = true;
      }else{
        isAdmin = false;
      }

      //Add existing users to user list


      for (int i=1;i<userList.length;i++) {
        if (userList[i] != null && ! userList[i].equals("")) {
          users.add(new User(userList[i], loc));
        }
      }

    }
    else{
      firstTime = true;
    }

    errorMessage = "";
  }

  public void loginAdmin() {
    users.get(0).login();
  }

  public boolean first(){
    return firstTime;
  }
  //Attempt user login
  public void check(String nm, KeyMap p) throws IOException {
    boolean userExists = false;
    User u;

    //Check if the user exists
    for (User us : users) {
      if (us.getUsername().equals(nm)) {
        userExists = true;
      }
    }

    u = get(nm);

    //If it doesn't, throw error
    if (! userExists) {
      errorMessage = "Invalid Username";
      Log.printErr("Invalid username");
      return;
    }

    //Check if the user used the correct typing pattern
    if (u.verify(p)) {
      //If they did, login and learn the users pattern more
      u.login();
      u.getPass().learn(p);
      Log.print("Success");
      errorMessage = "Nil";
    } else {
      //Otherwise, berate them for being stupid
      errorMessage = "Incorrect typing pattern";
      Log.printErr("Your typing pattern does not match the one on file");
    }
  }

  //Create a new user and add it to the user list
  public void newUser(String nm, KeyMap[] pass) {
    String jiao = nm;

    for (User us : users) {
      if (us.getUsername().equals(nm)) {
        errorMessage = "This username is already taken, please choose another.";
        return;
      }
    }

    //Create the new user
    User n = new User(nm, pass[0], loc);

    //Update its typing pattern
    for (int i=1; i<6; i++) {
      n.learn(pass[i]);
    }
    errorMessage = "Nil";
    //Add it to the user list
    users.add(n);

    //Login to the new user
    n.login();
  }

  //Return the user in the user list with a given username
  public User get(String nm) {
    for (User u : users) {
      if (u.getUsername().equals(nm)) {
        return u;
      }
    }
    return null;
  }

  public User getL() {
    for (User u : users) {
      if (u.loggedIn()) {
        return u;
      }
    }
    return null;
  }

  //Print a file with the list of usernames and have every user print their data file
  public void close() throws IOException {
    File ha = new File(loc+"/Users.txt");
    PrintWriter userListOut = new PrintWriter(ha);

    if(isAdmin){
      userListOut.println(Encrypt.eString("isAdmin"));
    }

    for (int i=0; i<users.size(); i++) {
      userListOut.println(users.get(i).getUsername());
      users.get(i).PrintData();
    }
    userListOut.flush();
    userListOut.close();

    Log.print(this, "User Data Saved");
  }

  //Check if the userlist is empty
  public boolean empty() {
    if (users.size() == 0) {
      return true;
    }

    return false;
  }

  //Get the username of the user that is logged in
  public String loggedInNm() {
    for (User i : users) {
      if (i.loggedIn()) {
        return i.getUsername();
      }
    }

    return "Joe Smith";
  }

  public int loggedInSng() {
    for (User i : users) {
      if (i.loggedIn()) {
        return i.getSong();
      }
    }

    return 3;
  }

  public String getErr(){
    return errorMessage;
  }

  public void logoutCurrent(){
    for (User i : users) {
      if (i.loggedIn()) {
        i.logout();
      }
    }
  }

  public boolean ad(){
    return isAdmin;
  }

  public void loginTo(String us){
    //Log.print("|"+us+"|");
    errorMessage = "Nil";
    get(us).login();
  }
}