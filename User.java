import java.util.*;
import java.io.*;

public class User implements Serializable{
  private transient Password pass;
  private String username;
  private boolean logged;
  private String loc;
  private int song;
  private String encPass;

  //Creating a new user constructor
  public User(String nm, KeyMap p, String l) {
    pass = new Password(p);
    username = nm;
    logged = false;
    loc = l;
    song = (int)(Math.random() * 4);
    //Log.print(loc);
  }

  //returns Password
  public Password getPass() {
    return pass;
  }

  //Returns username
  public String getUsername() {
    return username;
  }

  //Returns whether or not user is logged in
  public boolean loggedIn() {
    return logged;
  }

  //Verifies typing pattern matches password
  public boolean verify(KeyMap other) {
    return pass.verify(other);
  }

  //Updates stored typing pattern
  public void learn(KeyMap other) {
    pass.learn(other);
  }

  //Logs in
  public void login() {
    logged = true;
    Log.print("Hello " + username + ", You have successfully logged in!");
  }

  //Logs out
  public void logout(){
    logged =false;
    Log.print("Logging out "+username+". User data saved.");
  }

  //Sends user data to a text file
  public void readyToSave(){
    encPass = pass.print();
  }

  //Uploads user data from a file
  public void repairData() throws IOException {
    String[] lines = encPass.split("\n");

    KeyMap tem = new KeyMap();

    //Seperate tolerance and acceptableErrors
    String[] mods = lines[0].split(":");

    //Split up data based on format
    for (int i=1; i<lines.length; i++) {
      String dec = Encrypt.bannanas(lines[i]);
      dec = dec.substring(0,dec.length());
      String[] oldnew = dec.split("\\+");
      String[] osections = null;
      String[] nsections = null;


      if(oldnew[0].length()>=1){
        oldnew[0] = oldnew[0].substring(0,oldnew[0].length()-1);


        osections = oldnew[0].split("/");

        nsections = oldnew[1].split("/");

        String[] ochars = osections[0].split(":");
        String[] ovals = osections[1].split(":");

        String[] nchars = nsections[0].split(":");
        String[] nvals = nsections[1].split(":");


        //Add data to password
        tem.addThis(ochars[0].charAt(0), ochars[1].charAt(0), Long.parseLong(ovals[0]), Long.parseLong(ovals[1]), nchars[0].charAt(0), nchars[1].charAt(0), Long.parseLong(nvals[0]), Long.parseLong(nvals[1]));
      }

    }

    //Pass values to new password
    pass = new Password(tem, Long.parseLong(mods[0]), Integer.parseInt(mods[1]));
  }

  public int getSong(){return song;}

  public void changeName(String n){
    username = n;
  }
  public void changeSong(int s){
    song = s;
  }
}
