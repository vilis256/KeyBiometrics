import java.util.*;

void mouseClicked() {
  clickAll(mouseX, mouseY);
  if (!loggedIn) {
    if (login.active()) {
      login.click(mouseX, mouseY);
    }
    if (newUser.active()) {
      newUser.click(mouseX, mouseY);
    }
  }
}

void mouseWheel(MouseEvent event) {
  float a = event.getCount();
  log.scroll(a);
}

void keyTyped() {
  if (key == ENTER || key == RETURN) {
    if(mainMenu && !loginMenu && !nUserMenu && users.ad()){
      log.process();
    }
    return;
  }

  if(mainMenu && !loginMenu && !nUserMenu && users.ad()){
    if(key != DELETE && key != BACKSPACE){
      log.type(key);
    }
    else{
      log.delete();
    }
    
  }

  if (login.active()) {
    if (key != DELETE && key != BACKSPACE) {
      login.type(key);
    } else {
      login.del();
    }

    if (login.complete()) {
      login.hide();
      loginMenu = false;
      loggedIn = true;
      try {
        users.check(login.user(), signature);
        if (users.getErr().equals("Nil") && ! mute) {
          track[songPlaying].pause();
          homeSong[users.loggedInSng()].rewind();
          homeSong[users.loggedInSng()].play();
        } else if (! mute) {
          track[songPlaying].pause();
          track[8].play();
        }
        log.clear();
        log.domain("System");
        log.msg("Login Success! Welcome " + users.loggedInNm());
        log.command("cd User");
        log.domain("User");
        log.command("_");

        if (users.loggedInNm().equals("Joe Smith")) {
          log.clear();
          log.domain("Error");
          log.msg(users.getErr());
          log.domain("System");
          log.command("_");
        }
      }
      catch(IOException e) {
        Log.printErr(e, 82);
      }
    }
  } else if (newUser.active()) {
    if (key != DELETE && key != BACKSPACE) {
      newUser.type(key);
    } else {
      newUser.del();
    }

    if (newUser.allComplete()) {
      newUser.hide();
      nUserMenu = false;
      loggedIn = true;
      users.newUser(newUser.user(), learnPattern);
      log.clear();
      if (users.getErr().equals("Nil")) {
        log.domain("System");
        log.msg("Succesfully created user! Welcome " + users.loggedInNm());
        log.command("cd User");
        log.domain("User");
        log.command("_");
      } else {
        log.domain("Error");
        log.msg(users.getErr());
      }
    }
  }
}

public void mainMenu() {
  log.domain("System");

  if (users.first()) {
    image(instruct, 0, 0, width, height);
  } else {
    image(back, 0, 0, width, height);
  }
  if (!loginMenu && !nUserMenu) {
    if (! users.first()) {
      theButtons.get(0).show();
    }
    theButtons.get(1).show();
  }

  log.show();

  if (theButtons.get(2).clicked()) {
    quit();
  }
  if (theButtons.get(0).clicked()) {
    log.command("Running Login Menu...");
    loginMenu = true;
    theButtons.get(0).deactivate();
    theButtons.get(1).deactivate();
    theButtons.get(5).activate();
  }
  if (theButtons.get(1).clicked()) {
    log.command("Running Create User...");
    nUserMenu = true;
    theButtons.get(0).deactivate();
    theButtons.get(1).deactivate();
    theButtons.get(5).activate();
  }
  if (loginMenu) {
    if (theButtons.get(3).clicked()) {
      loginPass.reset();
      signature.clear();
      login.clearPass();
    }
  }
}

public void userHome() {
  log.domain("User");
  //Background
  image(home, 0, 0, width, height);
  fill(255, 0, 0);
  textSize(30);

  //Log display
  if (! users.loggedInNm().equals("Joe Smith")) {
    text(users.loggedInNm(), width/6+20, 140);

    log.show();
  } else {

    //Fail background
    //fill(15, 110, 125, 100);
    image(failBack, 0, 0);
    log.changeC(0);
    //rect(0, 0, width, height);

    log.show();
  }

  //Click the quit button
  if (theButtons.get(2).clicked()) {
    quit();
  }

  //Hide everything but quit button
  theButtons.get(0).deactivate();
  theButtons.get(1).deactivate();
  theButtons.get(5).deactivate();
  theButtons.get(2).show();

  //Show Logout button
  theButtons.get(4).activate();
  theButtons.get(4).show();

  //Logout Button Function
  if (theButtons.get(4).clicked()) {
    logoutMain();
    log.changeC(color(45, 200, 210));
  }
}

void keyPressed() {
  if (loginMenu) {
    if (login.editingPass()) {
      if (loginPass.bRec()) {
        if (key == DELETE || key == BACKSPACE) {
          loginPass.reset();
        } else {
          loginPass.stopBRec(key);
          signature.addThis(loginPass.finish());
          loginPass.reset();
          loginPass.startHRec(key);
        }
      } else if (! loginPass.hRec()) {
        loginPass.startHRec(key);
      }
    }
  }

  if (nUserMenu) {
    if (newUser.editingLearn() > -1) {
      int ind = newUser.editingLearn();
      if (nuLearn[ind].bRec()) {
        if (key == DELETE || key == BACKSPACE) {
          nuLearn[ind].reset();
        } else {
          nuLearn[ind].stopBRec(key);
          learnPattern[ind].addThis(nuLearn[ind].finish());
          nuLearn[ind].reset();
          nuLearn[ind].startHRec(key);
        }
      } else if (! nuLearn[ind].hRec()) {
        nuLearn[ind].startHRec(key);
      }
    }
  }
}

void keyReleased() {
  if (loginMenu) {
    if (login.editingPass()) {
      loginPass.stopHRec();
      loginPass.startBRec();
    }
  }

  if (nUserMenu) {
    int ind = newUser.editingLearn();
    if (newUser.editingLearn()>-1) {
      nuLearn[ind].stopHRec();
      nuLearn[ind].startBRec();
    }
  }
}

void clickAll(int x, int y) {
  for (Button i : theButtons) {
    i.click(x, y);
  }
  for (Button i : lesButtons) {
    i.click(x, y);
  }
}

color invC(color a) {
  float r=red(a), g=green(a), b=blue(a);
  color ret=color(255-r, 255-g, 255-b);
  return ret;
}
