import java.util.*;
import java.io.*;

public class KeyMap {
  ArrayList<KeyMotion> current;
  ArrayList<KeyMotion> original;
  ArrayList<Long> tolers;

  //Constructor
  public KeyMap() {
    current= new ArrayList<KeyMotion>();
    original= new ArrayList<KeyMotion>();
    tolers= new ArrayList<Long>();
  }

  //Add a KeyMotion to the database
  public void addThis(char a, char b, long c, long d) {
    long c2 =c;
    long d2 = d;
    if (! have(new KeyMotion(a, b, c, d))) {
      if(c > 200){c2=200;}
      if(d > 500){d2=500;}
      original.add(new KeyMotion(a, b, c2, d2));
      current.add(new KeyMotion(a, b, c2, d2));
    } else {
      learn(new KeyMotion(a, b, c, d));
    }
  }

  public void addThis(char a, char b, long c, long d, char e, char f, long g, long h) {

    original.add(new KeyMotion(a, b, c, d));
    current.add(new KeyMotion(e, f, g, h));
  }

  public void addThis(KeyMotion a) {
    if (! have(a)) {
      original.add(a);
      current.add(a);
    } else {
      learn(a);
    }
  }

  //Equalize all KeyMotions with each other
  public void learn(KeyMap other) {
    for (KeyMotion i : other.map()) {
      if (have(i)) {
        //Equalize if the KeyMotion exists and its not to far from original
        if(original.get(getLoc(i.getKey())).equalize(i).compareNum(original.get(getLoc(i.getKey()))) <= 100){
          current.set(getLoc(i.getKey()), original.get(getLoc(i.getKey())).equalize(i));
        }
      } else {
        //Add it to the database if it doesn't
        addThis(i);
      }
    }
  }

  public void learn(KeyMotion ke) {
    if(original.get(getLoc(ke.getKey())).equalize(ke).compareNum(original.get(getLoc(ke.getKey()))) <= 100){
      current.set(getLoc(ke.getKey()), original.get(getLoc(ke.getKey())).equalize(ke));
    }
  }

  //Compare two KeyMaps, return the number of differences
  public int compare(KeyMap o, long t) {
    int ret = 0;

    //Get a factor of how many times faster/slower
    double div;
    try{
      div = o.map().get(0).div(current.get(getLoc(o.map().get(0).getKey())));
    }catch(IndexOutOfBoundsException e){
      div = 1;
    }


    for (int i=0; i<o.map().size(); i++) {

      //If the Motion is not in the database, remove it
      if (! have(o.map().get(i))) {
        o.map().remove(i);
        ret ++;
      }

      if (i >= o.map().size()) {
        break;
      }

      //Boolean to check if error was made
      boolean boo = false;

      //If the two motions aren't within tolerance of each other
      if (! current.get(getLoc(o.map().get(i).getKey())).match(o.map().get(i), t)) {
        //Print the problem
        Log.print(o.map().get(i).getKey());
        Log.print(current.get(getLoc(o.map().get(i).getKey())).diff(o.map().get(i), t));

        //Add an error
        ret ++;
        boo = true;
      }

      //If the motions are within factor of each other and an error was made, remove the error
      if (o.map().get(i).fact(current.get(getLoc(o.map().get(i).getKey())), div) && boo) {
        ret --;
        boo = false;
        Log.print("Error overridden by factor");
      }
      boo = false;
    }

    return ret;
  }

  //Have all Motions return data
  public String print() {
    String ret = "";

    for (int i=0; i<current.size(); i++) {
      int spacez = 20-original.get(i).print().length();

      ret += original.get(i).print() + "~\\+~" + current.get(i).print() + "~\n~";
    }

    return ret;
  }

  //Does this KeyMotion exist in the database
  public boolean have(KeyMotion o) {
    for (KeyMotion i : current) {
      if (i.getKey().equals(o.getKey())) {
        return true;
      }
    }

    return false;
  }

  public int getLoc(String val) {
    for (int i=0; i<current.size(); i++) {
      if (current.get(i).getKey().equals(val)) {
        return i;
      }
    }

    return 1;
  }

  public ArrayList<KeyMotion> map() {
    return current;
  }

  public void rem() {
    current.remove(current.size()-1);
    original.remove(original.size()-1);
    tolers.remove(tolers.size()-1);
  }

  public void clear() {
    current.clear();
    original.clear();
    tolers.clear();
  }
}
