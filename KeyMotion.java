import java.util.*;
import java.io.*;

public class KeyMotion {
  private char from;
  private char to;

  private long timeHeldF;

  private long between;

  //Main constructor
  public KeyMotion(char f, char t, long hF, long tN) {
    from = f;
    to = t;
    timeHeldF = hF;
    between = tN;
  }

  //See if difference is within tol
  public boolean match(KeyMotion k,long tol) {
    return compare(k.getHeld(),timeHeldF,tol) && compare(k.getBetween(),between,tol);
  }

  //Get report on errors
  public String diff(KeyMotion k,long tol){
    return String.valueOf(k.getHeld()-timeHeldF) + "  :  "+ String.valueOf(k.getBetween()-between)+"\nOff by: "+String.valueOf(tol-Math.abs(k.getHeld()-timeHeldF)) + "  :  "+ String.valueOf(tol-Math.abs(k.getBetween()-between));
  }

  //Get factor value
  public double div(KeyMotion k){
    if(between != 0 && timeHeldF != 0){
     return meanD(k.getBetween()/between,k.getHeld()/timeHeldF);
    }
    else{
      return 0;
    }
  }

  //Check if div is within factor value
  public boolean fact(KeyMotion k, double d){
   return compareD(k.getBetween(),between,d) && compareD(k.getHeld(),timeHeldF,d);
  }

  //Compare sizes
  private boolean compare(long a,long b,long t){
    return Math.abs(a-b) <= t;
  }

  public long compareNum(KeyMotion ot){
    long tem1 = Math.abs(ot.getHeld() - timeHeldF);
    long tem2 = Math.abs(ot.getBetween() - between);

    return mean(tem1,tem2);
  }

  //Compare with factor
  private boolean compareD(long a,long b,double d){
    if(b != 0){
   return Math.abs((a/b)-d) <= 0.1;
    }
    else{
      return false;
    }
  }

  //Get methods
  public char getFrom() {
    return from;
  }

  public char getTo() {
    return to;
  }

  //Key value
  public String getKey(){
    return String.valueOf(from) + String.valueOf(to);
  }

  public long getBetween() {
    return between;
  }

  //Average two KeyMotions
  public KeyMotion equalize(KeyMotion o){
    long tem1 = mean(timeHeldF,o.getHeld());
    long tem2 = mean(between,o.getBetween());

    return new KeyMotion(from,to,tem1,tem2);
  }

  //Average method
  public long mean(long... a) {
    long sum = 0;
    for (long i : a) {
      sum+=i;
    }
    return sum/a.length;
  }

  //Average method return double
  public double meanD(long... a) {
    double sum = 0;
    for (long i : a) {
      sum+=(double)i;
    }
    return sum/a.length;
  }

  public long getHeld() {
    return timeHeldF;
  }

  //Return motion data
  public String print() {
    return from+"~:~"+to+"~/~"+timeHeldF+"~:~"+between;
  }
}
