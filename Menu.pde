class Menu {
  private String title;
  private ArrayList<Field> fields;
  private boolean active;
  private int w, h;
  Menu(String t, ArrayList<Field> f) {
    title = t;
    fields = f;
    active = false;
  }
  Menu(String t) {
    title = t;
    fields = new ArrayList<Field>();
    active = false;
  }
  void siz(int a, int b) {
    w=a;
    h=b;
  }
  void display() {
    active = true;

    fill(170, 150, 170, 150);
    rect(0, 0, width, height);

    fill(180, 150);
    rectMode(CENTER);
    rect(width/2, height/2, 2*width/3, 2*height/3);
    rectMode(CORNER);
    textSize(40);
    fill(0);
    int titPos = (width/2)-((title.length() * 30)/2);
    text(title, titPos, 50);


    for (Field i : fields) {
      i.show();
    }
  }

  boolean active() {
    return active;
  }

  void hide() {
    active = false;

    for (Field i : fields) {
      i.hide();
    }
  }

  void click(float a, float b) {
    for (Field i : fields) {
      if (i.inside(a, b)) {
        i.setEdit(true);
        for (Field j : fields) {
          if (j != i) {
            j.setEdit(false);
          }
        }
      }
    }
  }

  void type(char let) {
    for (Field i : fields) {
      if (i.edit()) {
        i.add(let);
      }
    }
  }

  void del() {
    for (Field i : fields) {
      if (i.edit()) {
        i.del();
      }
    }
  }

  void addField(Field e) {
    fields.add(e);
  }

  boolean editingPass() {
    for (Field i : fields) {
      if (i.name.equals("Type the phrase in the box") && i.edit()) {
        return true;
      }
    }
    return false;
  }

  public int editingLearn() {
    for (int i=0; i<fields.size(); i++) {
      if (fields.get(i).name.equals("Type the phrases in the boxes") || fields.get(i).name.equals("") && fields.get(i).edit()) {
        return i-1;
      }
    }
    return -1;
  }

  ArrayList<Field> fields() {
    return fields;
  }

  void resetAll(){
    for(Field i:fields){
      i.erase();
    }
  }
}

class LoginMenu extends Menu {
  private String[] phrases = {"the appsword is password", "the end is nigh", "hi my name is sam", "log in to login", "as seen on tv", "ask your doctor", "quidditch"};
  private final int ran = (int)random(phrases.length);
  private String phrase = phrases[ran];
  private int w, h;
  LoginMenu(String t, boolean s, int qx, int qy) {
    super(t);
    w=qx;
    h=qy;
    int xf=width/6+10;
    int yf=height/4+10;
    int wid=2*width/5 ;
    int hit=height/33;
    Field a = new Field("Username", true, xf, yf-40, wid, hit);
    addField(a);

    if (s) {
      Field b = new Field("Type the phrases in the boxes", true, xf, yf+40, wid, hit, phrases[0]);
      Field c = new Field("", true, xf, yf+80, wid, hit, phrases[1]);
      Field d = new Field("", true, xf, yf+120, wid, hit, phrases[2]);
      Field e = new Field("", true, xf, yf+160, wid, hit, phrases[3]);
      Field f = new Field("", true, xf, yf+200, wid, hit, phrases[4]);
      Field g = new Field("", true, xf, yf+240, wid, hit, phrases[5]);
      Field k = new Field("", true, xf, yf+280, wid, hit, phrases[6]);

      addField(b);
      addField(c);
      addField(d);
      addField(e);
      addField(f);
      addField(g);
      addField(k);
      ArrayList<Field> fields=fields();
      Field tempr=fields.remove(0);
      for(Field i:fields){
        float[] te=i.getDim();
        lesButtons.add(new Button("RESET",(int)te[0]+wid+20,(int)te[1]+5,12,color(255,0,0)));
      }
      fields.add(0,tempr);

    } else {
      Field b = new Field("Type the phrase in the box", true, xf, yf+40, 500, 25, phrase);
      addField(b);
    }
  }

  String user() {
    return fields().get(0).getContents();
  }

  void siz(int a, int b) {
    w=a;
    h=b;
  }

  boolean complete() {
    return fields().get(1).getContents().equalsIgnoreCase(phrase);
  }

  boolean complete(int fN) {
    return fields().get(fN).getContents().equalsIgnoreCase(phrases[fN-1]);
  }

  boolean allComplete() {
    for (int i=1; i<8; i++) {
      if (! complete(i)) {
        return false;
      }
    }

    return true;
  }

  void clearPass() {
    fields().get(1).erase();
  }
  void clearPass(int a) {
    fields().get(a+1).erase();
  }
}
